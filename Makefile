


.PHONY: build publish

build:
	python3 -m build

publish: clean build
	twine check dist/*
	twine upload dist/*

clean:
	rm -Rf dist
